SolidWorks Preview plugin 1.3.0.1 for Total Commander
=====================================================

 * License:
-----------

This software is released as freeware.


 * Disclaimer:
--------------

This software is provided "AS IS" without any warranty, either expressed or
implied, including, but not limited to, the implied warranties of
merchantability and fitness for a particular purpose. The author will not be
liable for any special, incidental, consequential or indirect damages due to
loss of data or any other reason.


 * Installation:
----------------

Open the plugin archive using Total Commander and installation will start.

The default configuration file sldpreview.ini is initialized during the very
first plugin initialization.


 * Update Remarks:
------------------

 o With version 1.2.0.0 the directory of sldpreview.ini changed from the plugin
   directory to the same directory as the initialization file (wincmd.ini) of Total
   Commander. However, if a sldpreview.ini is found in the plugin directory this
   sldpreview.ini is used instead.


 * Description:
---------------

SolidWorks� stores a preview bitmap for each configuration. This is typically
shown in the File-Open dialog of SolidWorks.

The SolidWorks Preview plugin shows the embedded preview bitmap of SolidWorks
Assembly, Drawing and Part Documents. It can also be used for the thumbnail
view of for Total Commander >= 6.5.

Extracting the preview bitmap does not require SolidWorks to be installed.

The background color can be set in section [SolidWorks Preview Settings] of
sldpreview.ini.


 * Examples:
------------

You can download some example SolidWorks files from
http://tbeu.totalcmd.net/solidworks/SolidWorks_SampleFiles.rar. Total download
size is about 913 kByte.


 * ChangeLog:
-------------

 o Version 1.3.0.1 (05.01.2013)
   - fixed memory leaks
   - added sources (Visual C++ 2010)
 o Version 1.3.0.0 (14.06.2012)
   - added full support of Unicode
   - fixed bitmap scaling according to display options (fit image to window, fit
     only larger image, center image)
   - fixed aspect ratio of extracted bitmap
   - fixed: extracted bitmap is no longer saved to a temporary file
   - removed options Width and Height from section [SolidWorks Preview Settings]
     of sldpreview.ini.
   - removed dependencies on external libraries, i.e. SW_ExtractBitmap[64].dll are
     no longer required and thus can be unregistered and deleted
   - removed support of WhereIsIt
 o Version 1.2.0.1 (25.09.2011)
   - fixed setting of background color
   - fixed: use unique temporary file name for extracted bitmap
   - removed dependency requirement for SW_ExtractBitmap64.dll
 o Version 1.2.0.0 (20.09.2011)
   - fixed registration of SW_ExtractBitmap.dll
   - changed directory of sldpreview.ini from plugin directory to same directory
     as the initialization file (wincmd.ini) of Total Commander
   - added 64 bit support (for Total Commander >= 8 beta 1)
 o Version 1.1.4.1 (06.09.2007)
   - fixed: return NULL in ListLoad() if extraction of preview bitmap fails
 o Version 1.1.4.0 (09.08.2007)
   - added: print preview bitmap (CTRL+P)
   - replaced SldWorks_ExtractBitmap.dll library by SW_ExtractBitmap.dll
 o Version 1.1.3.0 (13.04.2007)
   - fixed vertical scrolling
 o Version 1.1.2.0 (02.02.2007)
   - added: copy preview bitmap to clipboard (CTRL+C)
 o Version 1.1.1.0 (27.11.2006)
   - added support for new lister option: "Center images" (for Total Commander
     >= 7 beta 1)
   - removed option CenterImage from sldpreview.ini
 o Version 1.1.0.0 (25.11.2006)
   - added: avoid flickering of preview window when switching from one file to
     the next (for Total Commander >= 7 beta 2)
   - added option Debug in sldpreview.ini
 o Version 1.0.5.0 (18.01.2006)
   - added option BackColor in sldpreview.ini
   - fixed repainting if preview window was moved out of screen
   - fixed potential access violations
 o Version 1.0.4.0 (25.11.2005)
   - added support for WhereIsIt
 o Version 1.0.3.0 (17.11.2005)
   - added extensions ASM, ASMDOT, DRW, DRWDOT, PRT, PRTDOT, SLDDRT, SLDFLP
   - added scrolling
   - fixed: thumbnail view crashed if ListLoad() was not called before
 o Version 1.0.2.0 (12.11.2005)
   - added registration of SldWorks_ExtractBitmap.dll
 o Version 1.0.1.0 (11.11.2005)
   - added option CenterImage in sldpreview.ini
   - fixed: fit image to window in Quick View mode
 o Version 1.0.0.1 (10.11.2005)
   - first public version


 * References:
--------------

 o LS-Plugin Writer's Guide by Christian Ghisler
   - http://ghisler.fileburst.com/lsplugins/listplughelp2.1.zip


 * Trademark and Copyright Statements:
--------------------------------------

 o SolidWorks is Copyright � 1997-2012 by Dassault Syst�mes SolidWorks Corp.
   All rights reserved.
   - http://www.solidworks.com
 o Total Commander is Copyright � 1993-2012 by Christian Ghisler, Ghisler Software GmbH.
   - http://www.ghisler.com


 * Feedback:
------------

If you have problems, questions, suggestions please contact Thomas Beutlich.
 o Email: support@tbeu.de
 o URL: http://tbeu.totalcmd.net