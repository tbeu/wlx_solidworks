#define _STR(x) #x
#define STR(x) _STR(x)
#define PLUGIN_AUTHOR "Thomas Beutlich"
#define PLUGIN_VERSION_MAJOR 1
#define PLUGIN_VERSION_MINOR 3
#define PLUGIN_VERSION_MAINTENANCE 0
#define PLUGIN_VERSION_BUILD 1
#define PLUGIN_VERSION_NO PLUGIN_VERSION_MAJOR,PLUGIN_VERSION_MINOR,PLUGIN_VERSION_MAINTENANCE,PLUGIN_VERSION_BUILD
#define PLUGIN_VERSION "1.3.0.1"
